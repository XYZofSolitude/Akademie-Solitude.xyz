What/Why/How/forWho/When/Where could this (not)* be?

== #1 ?WHAT
Online platform for creative and critical collaborative work in/on/around 
experiences of Akademie Schloss Solitude. Named with distinctive XYZ 
as an opposite to the instructive ABC manual that future fellows receive. 
...

== #2 ?WHY
As a follow-up to shared thoughts, upcoming departures and Solitude changes...
...but also discussions about institutions, critique, commoning and openness.
Because it is sometimes hard to not have thoughts, ideas, proposals...
...and not to share them (with hope) they will be considered,
so that futures will be more adequate and maybe 
some shortcomings/issues will avoided.
...

== #3 ?HOW 
Maybe to try using 'radical' open-source software/content development model 
using maybe overly techno centric tools, but not only or exclusively so...
...sharing and caring should be central, with sustainability and applicability. 
Web domain Akademie-Solitude.xyz is a kind of present to Mr. Joly
(or at least it was presented that way in print-out folded/boxed with others),
but not so much as have him as Domain Name handler, 
but rather and offer to join in creating place for stories and discourses
that both follow his time and networks as well us help us create paths
out of physical buildings into more soft relations.
...

== #4 ?WHO 
Anyone who experienced Akademie Schloss Solitude in more then single and 
exceptional encounter/instance and has creative and critical contribution
to share and care over with others.
...

== #5 ?WHEN 
Whenever impulses of self expression give reach momentum and/or 
ideally when shared expressions can be articulated.
...

== #6 ?WHERE 
https://framagit.org/XYZofSolitude/Akademie-Solitude.xyz.git

*it is sometimes easier to define the negative 
and therefore more useful then the assumed precision of positive.